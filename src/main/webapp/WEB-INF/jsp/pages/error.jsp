<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="jumbotron">
    <div class="container">
        <h1 class="display-3">Sorry, an error occurred</h1>
        <p>Push the button to return to main page</p>
        <p><a class="btn btn-primary btn-lg" href="/" role="button">Main page</a></p>
    </div>
</div>