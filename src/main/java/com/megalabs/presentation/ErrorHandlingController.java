package com.megalabs.presentation;

import com.megalabs.presentation.helper.GlobalDefaultExceptionHandler;
import com.megalabs.presentation.helper.ModelAndViewHelper;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Shows error jsp if error occurred
 */
@Controller
public class ErrorHandlingController implements ErrorController {

    private static final String PATH = "/"+GlobalDefaultExceptionHandler.DEFAULT_ERROR_VIEW;

    @RequestMapping(value = PATH)
    public ModelAndView error() {
        return ModelAndViewHelper.getModelAndView(GlobalDefaultExceptionHandler.DEFAULT_ERROR_VIEW);
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}