package com.megalabs.presentation.helper;

import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 16.03.2016.
 */
public class ModelAndViewHelper {

    public static ModelAndView getModelAndView(String path) {
        return new ModelAndView("body", "path", getPath(path));
    }

    private static String getPath(String rawPath) {
        return "pages/" + rawPath + ".jsp";
    }
}
