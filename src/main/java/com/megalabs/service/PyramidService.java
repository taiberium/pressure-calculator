package com.megalabs.service;

/**
 * We use abstraction, so if wee need, any other implementation of abstraction could be used
 */
public interface PyramidService {

    double findWeightOnTopOfBlock(long element, long level);

    long findMaxPyramidLevel();

    long findMaxElement(long level);
}
