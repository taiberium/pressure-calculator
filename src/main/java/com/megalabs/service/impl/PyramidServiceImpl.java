package com.megalabs.service.impl;

import com.google.common.annotations.VisibleForTesting;
import com.megalabs.service.PyramidService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/*
* Solution based on the assumption that there is two-dimensional pyramid
* */
@Service
public class PyramidServiceImpl implements PyramidService {

    private static final long LEVEL_FIRST_BLOCK_NUM = 0;

    /*Better write like this:
    * ${pyramid.block.mass:100}
    * Then, if even value not found, we will have default 100 value
    * */
    @Value("${pyramid.block.mass}")
    private long singleBlockWeight;
    @Value("${pyramid.maxLevel}")
    private long maxPyramidLevel;   // if here level is 5 this mean that at the base of the pyramid 5 blocks (levels 0-4)

    @VisibleForTesting
    protected void setSingleBlockWeight(long singleBlockWeight) {
        this.singleBlockWeight = singleBlockWeight;
    }

    @VisibleForTesting
    protected void setMaxPyramidLevel(long maxPyramidLevel) {
        this.maxPyramidLevel = maxPyramidLevel;
    }

    @Override
    public long findMaxPyramidLevel() {
        return maxPyramidLevel;
    }

    @Override
    public long findMaxElement(long level) {
        return level;
    }

    //level counts from 0 from top to bottom
    //element counts from 0 from left to right
    // most highest block is on 0 level and number 0 element
    @Override
    public double findWeightOnTopOfBlock(long element, long level) {
        long levelMaxElement = findMaxElement(level);
        if (levelMaxElement < element || levelMaxElement == LEVEL_FIRST_BLOCK_NUM) {
            return 0;
        }
        // if first block
        if (element == LEVEL_FIRST_BLOCK_NUM) {
            return findRightBlockPressure(element, level);
        }
        // if last block of level
        if (element == levelMaxElement) {
            return findLeftBlockPressure(element, level);
        }
        // if not last and not first
        return findLeftBlockPressure(element, level) + findRightBlockPressure(element, level);
    }

    private double findLeftBlockPressure(long element, long level) {
        return (findWeightOnTopOfBlock(element - 1, level - 1) + singleBlockWeight) / 2;
    }

    private double findRightBlockPressure(long element, long level) {
        return (findWeightOnTopOfBlock(element, level - 1) + singleBlockWeight) / 2;
    }
}
