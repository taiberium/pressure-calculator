package com.megalabs.service.impl;

import org.junit.Before;
import org.junit.Test;

import java.time.Instant;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by User on 23.03.2016.
 */
public class PyramidServiceImplTest {

    private static final long DEFAULT_BLOCK_WEIGHT = 100;
    private static final long DEFAULT_PYRAMID_HEIGHT = 5;
    private static final double DELTA = 0;

    private PyramidServiceImpl pyramidService;

    @Before
    public void setup() {
        pyramidService = new PyramidServiceImpl();
        pyramidService.setMaxPyramidLevel(DEFAULT_PYRAMID_HEIGHT);
        pyramidService.setSingleBlockWeight(DEFAULT_BLOCK_WEIGHT);
    }

    @Test
    public void whenFindFirstBlockOnThirdLevelTopPressure() {
        double expectedWeight = 3D / 4 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(0, 2);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindFirstBlockOnFourthLevelTopPressure() {
        double expectedWeight = 1D / 2 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(0, 1);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindFirstBlockOnFifthLevelTopPressure() {
        double expectedWeight = 0;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(0, 0);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindSecondBlockOnThirdLevelTopPressure() {
        double expectedWeight = 3D / 2 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(1, 2);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindFirstBlockOnSecondLevelTopPressure() {
        double expectedWeight = 7D / 8 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(0, 3);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindSecondBlockOnSecondLevelTopPressure() {
        double expectedWeight = 17D / 8 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(1, 3);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindThirdBlockOnFirstLevelTopPressure() {
        double expectedWeight = 25D / 8 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(2, 4);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindFirstBlockOnFirstLevelTopPressure() {
        double expectedWeight = 15D / 16 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(0, 4);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindFifthBlockOnFirstLevelTopPressure() {
        double expectedWeight = 15D / 16 * DEFAULT_BLOCK_WEIGHT;

        double actualWeight = pyramidService.findWeightOnTopOfBlock(4, 4);

        assertEquals(expectedWeight, actualWeight, DELTA);
    }

    @Test
    public void whenFindSummOfGroundPressureThenShouldBeEqualToTotalMassOfAllBlocks() {
        double expectedWeight = 15 * DEFAULT_BLOCK_WEIGHT;
        int blockOnGroundLegsCount = 6;
        double summOfGroundPressure = 0;
        for (int i = 0; i < blockOnGroundLegsCount; i++) {
            summOfGroundPressure+=pyramidService.findWeightOnTopOfBlock(i, 5);
        }

        assertEquals(expectedWeight, summOfGroundPressure, DELTA);
    }

    /*
    * One million method calls executes faster than 60 ms
    * */
    @Test
    public void stressTest() {
        long beforeMethod = Instant.now().toEpochMilli();

        for (int i = 0; i < 1_000_000; i++) {
            pyramidService.findWeightOnTopOfBlock(3, 1);
        }

        long afterMethod = Instant.now().toEpochMilli() - beforeMethod;
        long oneSecond = Instant.now().toEpochMilli() - Instant.now().minusSeconds(1).toEpochMilli();
        assertTrue(afterMethod < oneSecond);
    }

}
